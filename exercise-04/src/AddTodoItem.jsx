import React, { useState } from 'react';
import styles from './AddTodoItem.module.css';

const AddTodoItem = ({ addNewTodo }) => {
  const [newTodo, setNewTodo] = useState('');

  return (
    <div className={styles.addTodoItem}>
      <label htmlFor="newTodo">Description</label>
      <input
        type="text"
        id="newTodo"
        value={newTodo}
        onChange={(event) => {
          setNewTodo(event.target.value);
        }}
      />
      <button
        onClick={() => {
          addNewTodo(newTodo);
          setNewTodo('');
        }}
      >
        Add
      </button>
    </div>
  );
};

export default AddTodoItem;
