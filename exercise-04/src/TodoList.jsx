import React from 'react';
import styles from './TodoList.module.css';

const TodoList = ({ items, onTodoStatusChange, removeTodo }) => {
  return (
    <div>
      {items.map((item, index) => {
        return (
          <div
            key={index}
            className={
              item.isComplete
                ? `${styles.todoItem} ${styles.completed}`
                : `${styles.todoItem} ${styles.uncompleted}`
            }
          >
            <input
              id={index}
              type="checkbox"
              value={item.description}
              checked={item.isComplete}
              onChange={() => {
                onTodoStatusChange(index, item.isComplete);
              }}
            />
            <span> {item.description}</span>
            {item.isComplete ? <span> (Done!)</span> : null}
            <button
              onClick={() => {
                removeTodo(index);
              }}
            >
              Remove
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default TodoList;
