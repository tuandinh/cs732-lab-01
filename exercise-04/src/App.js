import { useState } from 'react';
import TodoList from './TodoList';
import AddTodoItem from './AddTodoItem';

// A to-do list to use for testing purposes
const initialTodos = [
  { description: 'Finish lecture', isComplete: true },
  { description: 'Do homework', isComplete: false },
  { description: 'Sleep', isComplete: true },
];

function App() {
  const [todos, setTodos] = useState(initialTodos);

  const onTodoStatusChange = (clickedItemIndex, status) => {
    setTodos(
      todos.map((item, index) => {
        if (index === clickedItemIndex) {
          const updatedItem = { ...item, isComplete: !status };
          return updatedItem;
        }
        return item;
      })
    );
  };

  const addNewTodo = (newTodo) => {
    setTodos([...todos, { description: newTodo, isComplete: false }]);
  };

  const removeTodo = (clickedTodoIndex) => {
    setTodos(
      todos.filter((item, index) => {
        if (index !== clickedTodoIndex) {
          return item;
        }
      })
    );
  };

  return (
    <div>
      <div>
        <h1>My todos</h1>
        <TodoList
          items={todos}
          onTodoStatusChange={onTodoStatusChange}
          removeTodo={removeTodo}
        />
      </div>
      <div>
        <h1>Add item</h1>
        <AddTodoItem addNewTodo={addNewTodo} />
      </div>
    </div>
  );
}

export default App;
