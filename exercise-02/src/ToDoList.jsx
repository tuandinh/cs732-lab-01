import React from 'react';

const TodoList = ({ items }) => {
  return (
    <ul>
      {items.map((item, index) => {
        return <li key={index}>{item}</li>;
      })}
    </ul>
  );
};

export default TodoList;
